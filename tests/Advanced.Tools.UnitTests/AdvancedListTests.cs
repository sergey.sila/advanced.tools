using FluentAssertions;
using System;
using Xunit;

namespace Advanced.Tools.UnitTests
{
    public class AdvancedListTests
    {
        [Fact]
        public void Should_Be_Initialized_With_Empty_Value()
        {
            //Arrange
            //Act
            var advancedList = new AdvancedList<int>();

            //Assert
            advancedList.Count.Should().Be(3);
        }

        [Fact]
        public void Should_Be_Able_ToMultiple_Values()
        {
            //Arrange
            var advancedList = new AdvancedList<int>();
            //Act
            advancedList.Add(5);
            advancedList.Add(10);
            advancedList.Add(15);
            advancedList.Add(20);

            //Assert
            advancedList[0].Should().Be(5);
            advancedList[1].Should().Be(10);
            advancedList[2].Should().Be(15);
            advancedList[3].Should().Be(20);
        }

        [Fact]
        public void Should_Be_Able_To_Remove_Value()
        {
            //Arrange
            var advancedList = new AdvancedList<int>();

            //Act
            advancedList.Add(5);
            advancedList.Add(10);
            advancedList.Add(15);
            advancedList.Add(20);
            advancedList.Add(25);
            advancedList.Add(30);
            advancedList.Remove(15);

            //Assert
            advancedList[0].Should().Be(5);
            advancedList[1].Should().Be(10);
            advancedList[2].Should().Be(20);
            advancedList[3].Should().Be(25);
            advancedList[4].Should().Be(30);
            advancedList[5].Should().Be(0);
            advancedList[6].Should().Be(0);
        }

        [Fact]
        public void Should_Be_Able_To_Add_Range_To_Array()
        {
            //Arrange
            var advancedList = new AdvancedList<int>();

            //Act
            advancedList.Add(5);
            advancedList.Add(10);
            advancedList.Add(15);
            advancedList.AddRange(1, 2, 3, 4);

            //Assert
            advancedList[0].Should().Be(5);
            advancedList[1].Should().Be(10);
            advancedList[2].Should().Be(15);
            advancedList[3].Should().Be(1);
            advancedList[4].Should().Be(2);
            advancedList[5].Should().Be(3);
            advancedList[6].Should().Be(4);
        }

        [Fact]
        public void Should_Be_Able_To_Add_Range_To_Empty_Array()
        {
            //Arrange
            var advancedList = new AdvancedList<int>();

            //Act
            advancedList.AddRange(1, 2, 3, 4);

            //Assert
            advancedList[0].Should().Be(1);
            advancedList[1].Should().Be(2);
            advancedList[2].Should().Be(3);
            advancedList[3].Should().Be(4);
        }
        [Fact]
        public void Should_Be_Able_To_Reverse_Array()
        {
            //Arrange
            var advancedList = new AdvancedList<int>();

            //Act
            advancedList.Add(5);
            advancedList.Add(10);
            advancedList.Add(15);
            advancedList.Add(20);
            advancedList.Reverse();
            //Assert
            advancedList[0].Should().Be(20);
            advancedList[1].Should().Be(15);
            advancedList[2].Should().Be(10);
            advancedList[3].Should().Be(5);
        }

        [Fact]
        public void Should_Be_Able_To_Remove_Range_From_Array()
        {
            //Arrange
            var advancedList = new AdvancedList<int>();

            //Act
            advancedList.Add(5);
            advancedList.Add(10);
            advancedList.Add(15);
            advancedList.Add(20);
            advancedList.Add(25);
            advancedList.RemoveRange(5, 20);

            //Assert
            advancedList[0].Should().Be(10);
            advancedList[1].Should().Be(15);
            advancedList[2].Should().Be(25);
        }

        [Fact]
        public void Should_Not_Be_Able_To_Remove_Invalid_Range_From_Array()
        {
            //Arrange
            var advancedList = new AdvancedList<int>();

            //Act
            advancedList.Add(5);
            advancedList.Add(10);
            Action RemoveAction = () => advancedList.RemoveRange(8);

            //Assert
            RemoveAction.Should().NotThrow();
        }
    }
}
