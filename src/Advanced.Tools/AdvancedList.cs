﻿using System;
using System.Net;

namespace Advanced.Tools
{
    public class AdvancedList<T>
    {
        private T[] InnerArray;

        private int Size;
        public int Count => InnerArray.Length;

        public AdvancedList()
        {
            InnerArray = new T[3];
            Size = 0;
        }

        public T this[int index]
        {
            get => InnerArray[index];
        }

        public void Add(T item)
        {
            HandleSize();
            InnerArray[Size] = item;
            Size++;

        }

        private void HandleSize(int size = 0, double k = 1.4)
        {
            if (size > 0)
            {
                var newSize = size;
                var newList = new T[newSize];
                InnerArray.CopyTo(newList, 0);
                InnerArray = newList;
            }

            else if (Size == Count)
            {

                var newSize = (int)(Size * k);
                var newList = new T[newSize];
                InnerArray.CopyTo(newList, 0);
                InnerArray = newList;
            }
        }

        public void Remove(T item)
        {
            var itemIndex = Array.IndexOf(InnerArray, item);
            if (itemIndex == -1)
            {
                return;
            }
            else
            {
                var copyLength = itemIndex;
                if (Size == Count)
                {
                    copyLength++;
                }
                Array.ConstrainedCopy(InnerArray, itemIndex + 1, InnerArray, itemIndex, Size - copyLength);
                InnerArray[--Size] = default;
            }
        }

        public void AddRange(params T[] inputArray)
        {
            var newSize = Count + inputArray.Length;
            HandleSize(size: newSize);
            inputArray.CopyTo(InnerArray, Size);
        }

        public void Reverse()
        {
            T temp;
            var midValue = Count / 2;
            Size = Count - 1;
            for (int i = 0; i < midValue; i++)
            {
                temp = InnerArray[i];
                InnerArray[i] = InnerArray[Size - i];
                InnerArray[Size - i] = temp;
            }
        }

        public void RemoveRange(params T[] inputArray)
        {
            foreach (var item in inputArray)
            {
                Remove(item);
            }
        }
    }
}
